$(document).ready(function(){
    $( "#arriveDate" ).datepicker(
        {
            dateFormat: 'mm-dd',
            minDate: new Date(2025, 6,13),
            maxDate: new Date(2025, 6, 26)
        });

    $( "#leaveDate" ).datepicker(
        {
            dateFormat: 'mm-dd',
            minDate: new Date(2025, 6,13),
            maxDate: new Date(2025, 6, 26)
        });
});