$(document).ready( function () {

    $("td").click(function () {
        getUsersByDay(this.innerHTML);
    });

    function getUsersByDay(day) {
        $.ajax({
            type: 'GET',
            url: 'getUsersByDay/' + day,
            success: function(data, textStatus, xhr) {

                $(".modal-title").empty();
                $(".modal-body").empty();

                $(".modal-title").append("Here's who will be at the Lake on 7/" + day);

                data.forEach(function (name) {
                    $(".modal-body").append("<li>" + name + "</li>");
                });

                $('#myModal').modal('toggle')
            }
        });

    }

} );