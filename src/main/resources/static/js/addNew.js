$(document).ready(function(){

	$("#addAnotherButton").click(function() {

		let newNameInputField = '<input type="text" class="newName mt-2 input-group form-control" placeholder="Name"/>';
		$("#nameRow").append(newNameInputField);

	});

	$("#clearButton").click(function () {
		$("input").val("");
	})

	$("#submitButton").click(function() {

		let name = $("#firstName").val();

		if( !name ) {
			$("#nameFieldAlert").css("visibility", "visible");
			$("#nameFieldAlert").html("Please enter a valid name");
			return;
		} else {
			$("#nameFieldAlert").css("visibility", "hidden");
		}

		let names = [];

		$('.newName').each(function(i, obj) {

			if(obj.value) {
				names.push(obj.value);
			}
		});

		$("#nameField").val(names);
		$("#formSubmitButton").click();

	});

});