$(document).ready(function () {
    $input = $("#select_date");
    $input.datepicker({
        format: 'dd MM yyyy'
    });
    $input.data('datepicker').hide = function () {};
    $input.datepicker('show');
});