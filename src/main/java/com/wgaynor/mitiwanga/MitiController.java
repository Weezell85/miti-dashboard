package com.wgaynor.mitiwanga;

import com.wgaynor.mitiwanga.repo.MitiRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.List;

@Controller
@AllArgsConstructor
public class MitiController {

    private final MitiRepository mitiRepository;

    @GetMapping("/addNew")
    public String addNewUser() {
        return "addNew";
    }

    @GetMapping("/miti")
    public String miti(Model model) {
        List<Miti> mitis = mitiRepository.findAll();
        model.addAttribute("miti", mitis);
        return "miti";
    }

    @GetMapping("/calendar")
    public String calendar(Model model) {
        List<Miti> mitis = mitiRepository.findAll();
        model.addAttribute("miti", mitis);
        return "checkCalendar";
    }

    @GetMapping("/calendarView")
    public String calendarView() {
        return "calendarView";
    }

    @PostMapping("/addNew")
    public String addNewUser(String name, String arriveDate, String leaveDate) {
        Arrays.stream(name.split(",")).forEach(e -> {
            mitiRepository.save(new Miti(e, arriveDate, leaveDate));
        });
        return "redirect:/miti";
    }

    @GetMapping("/edit_user/{id}")
    public String editUser(@PathVariable Long id, Model model) {
        Miti miti = mitiRepository.getReferenceById(id);
        model.addAttribute("miti", miti);
        return "editEntry";
    }

    @PostMapping("/edit_user")
    public String editUser(@ModelAttribute Miti miti) {
        mitiRepository.deleteById(miti.getId());
        mitiRepository.save(miti);
        return "redirect:/miti";

    }

    @GetMapping("/delete_user/{id}")
    public String deleteUser(@PathVariable Long id) {
        mitiRepository.deleteById(id);
        return "redirect:/miti";
    }
}
