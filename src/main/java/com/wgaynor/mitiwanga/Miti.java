package com.wgaynor.mitiwanga;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="MITI")
@NoArgsConstructor
public class Miti {

    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private long id;

    @Column(name="NAME")
    @Getter @Setter
    private String name;

    @Column(name="ARRIVE_DATE")
    @Getter @Setter
    private String arriveDate;

    @Column(name="LEAVE_DATE")
    @Getter @Setter
    private String leaveDate;

    public Miti(String name, String arriveDate, String leaveDate) {
        this.name = name;
        this.arriveDate = arriveDate;
        this.leaveDate = leaveDate;
    }

    public boolean willBeThere(int day) {
        if(arriveDate == null || leaveDate == null || arriveDate.isEmpty() || leaveDate.isEmpty()) return false;

        int arrive = Integer.parseInt(arriveDate.substring(arriveDate.lastIndexOf("-") + 1));
        int leave = Integer.parseInt(leaveDate.substring(leaveDate.lastIndexOf("-") + 1));

        return (day > arrive && day < leave);

    }

}
