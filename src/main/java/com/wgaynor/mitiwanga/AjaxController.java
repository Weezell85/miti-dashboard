package com.wgaynor.mitiwanga;

import com.wgaynor.mitiwanga.repo.MitiRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
public class AjaxController {

    private final MitiRepository repo;

    @GetMapping("/getUsersByDay/{day}")
    public ResponseEntity<List<String>> getUsersByDate(@PathVariable final String day) {
        List<String> mitis = repo.findUsersWithValidDates().
                stream().
                filter(miti -> miti.willBeThere(Integer.parseInt(day))).
                map(Miti::getName).
                collect(Collectors.toList());

        mitis.add("Test USer");

        return new ResponseEntity<>(mitis, HttpStatus.OK);
    }

}
