package com.wgaynor.mitiwanga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MitiwangaApplication {

	public static void main(String[] args) {

		final SpringApplication springApplication = new SpringApplication(MitiwangaApplication.class);

		/*
		*  Multiple ways to set profile.
		*  Setting dev will load application-dev.properties
		*  dev profile has H2 database configured.  Production has amazon mysql which is no longer available;
		*/
		springApplication.setAdditionalProfiles("dev");
		springApplication.run(args);

	}

}
