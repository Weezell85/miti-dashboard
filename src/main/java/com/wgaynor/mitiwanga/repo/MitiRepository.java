package com.wgaynor.mitiwanga.repo;

import com.wgaynor.mitiwanga.Miti;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MitiRepository extends JpaRepository<Miti, Long> {

    @Query("SELECT m.name FROM Miti m")
    List<String> findNames();

    @Query("SELECT m FROM Miti m where m.arriveDate is not null and m.leaveDate is not null")
    List<Miti> findUsersWithValidDates();

    @Query("FROM Miti m WHERE :day between m.arriveDate and m.leaveDate")
    List<String> findUsersByDay(@Param("day") String day);

}
